package br.ifsc.edu.blocodenotassql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class NotaController {
    NotasDao notasdao;
    Context context;

    public NotaController(Context context1) {
        this.context =  context1;
        notasdao = new NotasDao(context);
    }

    public void salvarNota(Nota nota) {
        notasdao.inserirNota(nota);
    }

    public String recuperaNota(){
        return notasdao.retornaNota();
    }

    public boolean VeSeIdExiste(int i) {
        if(notasdao.veId(i)>0){
            return true;
        }else{
            return false;
        }
    }

    public void updateNota(Nota nota) {
        notasdao.atualizaNota(nota);
    }
}
